<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>
  
<br /><br /><input type="button" value="Esempio Bottone" />
    <br /><br /><input class="btn btn-primary" type="button" value="Esempio Bottone" />
    <br /><br /><input class="btn btn-secondary" type="button" value="Esempio Bottone" />
    <br /><br /><input class="btn btn-danger" type="button" value="Esempio Bottone" />
    <br /><br /><input class="btn btn-danger btn-sm" type="button" value="Esempio Bottone" />
    <br /><br /><input class="btn btn-danger btn-lg" type="button" value="Esempio Bottone" />
    <br /><br /><input class="btn btn-primary btn-sm" type="button" value="Esempio Bottone" />
    

    <script src="js/jquery-3.6.0.js"> </script>
    <script src="js/popper.min.js"> </script>
    <script src="js/bootstrap.min.js" ></script>
   
  </body>
</html>