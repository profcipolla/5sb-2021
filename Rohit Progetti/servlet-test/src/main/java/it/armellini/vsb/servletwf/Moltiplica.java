package it.armellini.vsb.servletwf;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Moltiplica
 */
@WebServlet(urlPatterns = "/Moltiplica")
public class Moltiplica extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int numero1 = Integer.parseInt(request.getParameter(("t1")));
		int somma = numero1+numero1;
		PrintWriter out=response.getWriter();
		out.write(somma);
		out.flush();
		out.close();
		
	}

}
