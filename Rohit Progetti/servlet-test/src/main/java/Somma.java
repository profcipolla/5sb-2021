

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Somma
 */
@WebServlet(description = "Invia un saluto", urlPatterns = { "/Somma" })
public class Somma extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private static final String htmlBenvenuto=
    		   
    		  /* "<html>"
    	    	   +"<body>"
    	    				   +"<form action="Somma"method="get'>"
    	    				   +'<br><p>Enter 1st Nummber:<p><input type ='text'name='t1'>'
    	    				   +'</br>'
    	    				   +'<br><p>Enter 2st Number:<p><input type='text'name='t2'>'
    	    				   		+'</br>'
    	    				   +'<input type='submit'value='Add'>'
    	    				   +'</form>'
    	    				   +'</body>'
    	    				   +'</html>';
    		 
    		 */  
    		
    	    				   
    	    				     "<html>"
    			       +"<head>"
    				  +"<title>Progetto matematico</title>"
    				  +"</head>"
    				  +"<body>"
    				  +"<h1>Calcolo</h1>"
    				  +"Calcolo dei numeri � " //+ % S
    				  +"<script>"
    				  +"alert('Attenzione premere ok per ricevere la risposta : messaggio da Rohit Er meglio del peggio ');"
    				  +"</script>"
    				  +"</body>"
    			       + "</html>";
    	    				   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Somma() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int Numero1 = Integer.parseInt(request.getParameter("t1"));
		int Numero2 = Integer.parseInt(request.getParameter("t2"));
		
		int Somma =Numero1+Numero2;
		PrintWriter out = response.getWriter();
		
		
		out.write(String.format(htmlBenvenuto,Numero1,Numero2) );
		out.println(Somma);
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
