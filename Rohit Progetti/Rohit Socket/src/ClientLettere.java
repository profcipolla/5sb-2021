import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientLettere {
    private static final int PORT = 9999;
    private static final String Raddoppia = "Bella Macchina zio";
    
    public static void main(String[] a) {
        try {
            Socket clientSocket = new Socket("localhost", PORT);
            InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            PrintWriter out = new PrintWriter (clientSocket.getOutputStream());
        
            out.write(Raddoppia + "\n");
            out.flush();
            
            String risposta = in.readLine();
            System.out.println("RISPOSTA: " + risposta);
            
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            System.err.println("Errore durante la creazione del socket");
            System.err.println(e.getMessage());
        }
    }
}
