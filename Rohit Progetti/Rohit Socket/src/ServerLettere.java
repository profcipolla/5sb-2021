import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerLettere {
    private static final int PORT = 9999;
    
    public static void main(String[] a) {
        try {
            ServerSocket mioSocket = new ServerSocket(PORT);
            while(true) {
                System.out.println("Ascolto sulla porta " + PORT);
                Socket clientL = mioSocket.accept();
                System.out.println("connessione avvenuta");
                
                InputStreamReader isr = new InputStreamReader(clientL.getInputStream());
                // canale di input, da cui leggiamo i dati che il
                // client ci invia
                BufferedReader in = new BufferedReader(isr);
                
                // canale di output, invio i dai al client che si
                // � collegato
                PrintWriter out = new PrintWriter (clientL.getOutputStream());
            
                String messaggioRicevuto = in.readLine();
                
                String risposta = "";
                // raddoppio le lettere
                int len = messaggioRicevuto.length();
                for (int i = 0; i < len; i++) {
                    risposta += String.valueOf(messaggioRicevuto.charAt(i)) + String.valueOf(messaggioRicevuto.charAt(i));
                }
                
                out.write(risposta + "\n");
                System.out.println("... risposta inviata: " + risposta);
                out.flush();
                System.out.println("... chiudo connessione");
                out.close();
            } // while
        } catch (IOException e) {
            System.err.println("Errore durante la creazione del socket");
            System.err.println(e.getMessage());
        }
    }
}

