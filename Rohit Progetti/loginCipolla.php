<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/loginCipolla.css"/>
	<link rel="stylesheet" href="css/all.css"/>
    <title>Mio Progetto!</title>
  </head>
  <body>
	  <?php
// se è un post
$errore="";
$username="";
$password="";
$metodo=$_SERVER["REQUEST_METHOD"];
if ('POST'==$metodo){
	// prendo user e password per la verifica 
	$username=$_POST['email'];
$password=$_POST['password'];
// se verifa ok cambiopagina
if ( 'admin' == $username && 'prova'==$password){
header("Location: bootstrap.php");
}else{
	$errore = "Nome utente o password errati. Riprova";
}
}
echo"".$username;
echo"".$password;
	  ?>


  <div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Accedi</h3>
				
				<div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
				</div>
			</div>
			<div class="card-body">
				<form>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
                        
						<input  type="text" name="email" class="form-control"value = "<?=$username?>"placeholder="email">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" name ="password "class="form-control" placeholder="password">
					</div>
					<div class="row align-items-center remember">
						<input type="checkbox">Ricordami
					</div>
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">

                <div class="d-flex justify-content-center links">
					Non hai un account?<a href="#">Registrati</a>
				</div>
                <div class="d-flex justify-content-center">
					<a href="#">Hai dimenticato la password?</a>
				</div>
			</div>
		</div>
	</div>
</div>

  <script src="js/jquery-3.6.0.js"> </script>
    <script src="js/popper.min.js"> </script>
    <script src="js/bootstrap.min.js" ></script>
   
  </body>
</html>