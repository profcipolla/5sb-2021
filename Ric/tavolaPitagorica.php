<!doctype html>
<html>
<head>
<title>Tavola pitagorica</title>
</head>
<body>
<h1>Tavola pitagorica</h1>
<style>
        table {
            border: 2px solid orangered;
            margin: 10px 20px 5px 15px;
            width: 90%;
            border-collapse: collapse;
        }

        td,
        th {
            border: 1px solid gray;
            padding: 4px 10px;
        }

        form {
            border: 1px solid orangered;
            margin: 10px;
            padding: 8px;
        }
    </style>
<table>
<?php
$l = 10;
for($r=1; $r<=$l; $r++ )
{
echo ("<tr>");
for( $c=1; $c<=$l; $c++ )
{
$v = $r * $c;
echo ("<td>"); echo ($v); echo ("</td>\n");
}
echo ("</tr>\n");
}
?>
</table>
</body>
</html>
