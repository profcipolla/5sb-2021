package itis.armellini.server.socket;
import java.io.*;
import java.net.*;
public class MioServerSocket {
    private static int PORT = 9876;

    public static void main(String[] a) {
        //  boolean endCiclo = false;
        try {
            ServerSocket echoSocket = new ServerSocket(PORT);
            do {
                System.out.println("Sono in ascolto sulla porta " + PORT);
                Socket clientSocket = echoSocket.accept();

                BufferedReader inputBr = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintWriter outPw = new PrintWriter(clientSocket.getOutputStream());
                
                outPw.write("Per favore scrivi qualcosa...\n");
                outPw.flush();
                
                String messaggio = inputBr.readLine();
                while (null != messaggio && !"CIAO".equals(messaggio)) {
                    System.out.println("Ho letto " + messaggio);
                    
                    outPw.write("==> ECHO: " + messaggio + "\n");
                    outPw.flush();
                    
                    messaggio = inputBr.readLine();
                }
                
                if ("CIAO".equals(messaggio)) {
                    outPw.write("==> ARRIVEDERCI...\n");
                    outPw.flush();
                    clientSocket.close();
                    // endCiclo = true;
                }
                
            } while (true);
        } catch (IOException e) {
            System.out.println("Errore durante l'apertura del socket: " + e.getMessage());
        }
    }
}
