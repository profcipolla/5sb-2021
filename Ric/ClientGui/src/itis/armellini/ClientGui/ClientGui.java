package itis.armellini.ClientGui;


import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import javax.swing.*;


public class ClientGui extends JFrame implements ActionListener {
    private static int PORT = 9876;
    private static int WIDTH = 800;
    private static int HEIGHT = 300;
    private static String SRV_BENVENUTO = "Per favore scrivi qualcosa...";
    private static String SRV_CIAO = "CIAO";

    private JLabel lblMessaggio = new JLabel("Numero1");
    private JLabel lblMessaggio2 = new JLabel("Numero2");
    private JTextField tfMessaggio = new JTextField();
    private JTextField tfMessaggio2 = new JTextField();
    private JButton jbInvia = new JButton("Invia");
    private JLabel lblRisposta = new JLabel();

    public ClientGui() {
        setTitle("Client Socket");
        setBounds(20, 20, WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        LayoutManager mioLayout = new GridLayout(0, 1, 10, 10);
        setLayout(mioLayout);

        jbInvia.addActionListener(this);
        tfMessaggio.setPreferredSize(new Dimension(600, 20));
        JPanel pnlMessaggio = new JPanel();
        pnlMessaggio.setLayout(new GridLayout());
        pnlMessaggio.add(lblMessaggio);
        pnlMessaggio.add(lblMessaggio2);
        pnlMessaggio.add(tfMessaggio);
        pnlMessaggio.add(tfMessaggio2);
        add(pnlMessaggio);
        add(jbInvia);
        add(lblRisposta);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        lblRisposta.setText("");
        // System.out.println("Hai premuto sul pulsante INVIA");
        String messaggio = tfMessaggio.getText();
        String messaggio2 = tfMessaggio2.getText();
        int messaggio1 = Integer.parseInt(messaggio);
        int messaggio3= Integer.parseInt(messaggio2);
        int messaggiotot= messaggio1+messaggio3;
        String messaggiorisp = (""+messaggiotot); 
        
        // if () se il messaggio � stato inserito, qui sapete voi cosa fare

        try {
            Socket clientSocket = new Socket("localhost", PORT);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream());
            
            int passo = 0;
            String strIn = in.readLine();
            while(null != strIn) {
                System.out.print(strIn);
                // se ho ricevuto il messaggio d'invito del server...
                if (SRV_BENVENUTO.equals(strIn)) {
                    lblRisposta.setText(strIn + "\n");
                    // ... invio la stringa dell'utente
                    System.out.println("Invio il messaggio: " + messaggiorisp);
                    out.write(messaggiorisp + "\n");
                    out.flush();
                    passo++;
                } else if (1 == passo) {
                    lblRisposta.setText(lblRisposta.getText() +  strIn + "\n");
                    out.write(SRV_CIAO + "\n");
                    out.flush();
                    passo++;
                } else if (2 == passo) {
                    lblRisposta.setText(lblRisposta.getText() +  strIn + "\n");
                    passo++;
                    clientSocket.close();
                }
                strIn = in.readLine();
            }

        } catch (IOException exc) {
            System.out.println("ERRORE: " + exc.getMessage());
        }
    }
}
