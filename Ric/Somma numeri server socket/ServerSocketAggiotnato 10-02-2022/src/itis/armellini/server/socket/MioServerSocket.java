package itis.armellini.server.socket;
import java.io.*;
import java.net.*;
public class MioServerSocket {
    private static int PORT = 9876;

    public static void main(String[] a) {
        //  boolean endCiclo = false;
        try {
            ServerSocket echoSocket = new ServerSocket(PORT);
            do {
                System.out.println("Sono in ascolto sulla porta " + PORT);
                Socket remoteclientSocket = echoSocket.accept();

                BufferedReader inputBr = new BufferedReader(new InputStreamReader(remoteclientSocket.getInputStream()));
                PrintWriter outPw = new PrintWriter(remoteclientSocket.getOutputStream());
                
                outPw.write("Per favore scrivi qualcosa...\n");
                outPw.flush();
                String x = inputBr.readLine();
                String y = inputBr.readLine();
                int primonumero =Integer.parseInt(x);
                int secondonumero =Integer.parseInt(y);
                int somma = primonumero+ secondonumero;
                String messaggio = ""+somma; 
                
                while (null != messaggio && !"CIAO".equals(messaggio)) {
                    System.out.println("Ho letto " + messaggio);
                    
                    outPw.write("==> ECHO: " + messaggio + "\n");
                    outPw.flush();
                    
                    
                }
                
                if ("CIAO".equals(messaggio)) {
                    outPw.write("==> ARRIVEDERCI...\n");
                    outPw.flush();
                    remoteclientSocket.close();
                    // endCiclo = true;
                }
                
            } while (true);
        } catch (IOException e) {
            System.out.println("Errore durante l'apertura del socket: " + e.getMessage());
        }
    }
}
