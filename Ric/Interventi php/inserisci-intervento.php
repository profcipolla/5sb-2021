<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inserimento Intervento</title>
</head>

<body>
    <?php
        $host="127.0.0.1";
        $user="root";
        $password="";
        $db="studio-dentistico";

        $conn=mysqli_connect($host,$user,$password,$db);
        if(!$conn){
            die("errore durante la connessione: 404". mysqli_connect_error());
        }
        $sql_dentisti="select ID_DENTISTA,NOME_D,COGNOME_D"
        ." from dentisti"
        ." order by COGNOME_D,NOME_D";
        $res_dentisti = mysqli_query($conn,$sql_dentisti);
        if(!$res_dentisti){
            die("Errore durante l'esecuzione della query". $sql_dentisti );
        }
        $tab_dentisti = [];
        while ($riga_dentista = mysqli_fetch_assoc($res_dentisti)) {
            $tab_dentisti[$riga_dentista["ID_DENTISTA"]] = $riga_dentista["COGNOME_D"] . " ". $riga_dentista["NOME_D"];
        }
        $sql_pazienti="select COD_FISCALE_PAZIENTE,NOME_P,COGNOME_P" 
            . " FROM pazienti"
            . " order by COGNOME_P,NOME_P";
        $res_pazienti=mysqli_query($conn,$sql_pazienti);
        $tab_pazienti = [];
        while($riga_paziente = mysqli_fetch_assoc($res_pazienti)) {
            $tab_pazienti[$riga_paziente["COD_FISCALE_PAZIENTE"]] = $riga_paziente["COGNOME_P"]; 
        }
        echo "Metodo invocato " . $_SERVER["REQUEST_METHOD"];
        if ("POST" == $_SERVER["REQUEST_METHOD"]) {
            echo "<br />Devo salvare i dati";
            echo "<br /> id dentista " . $_POST["Dentista"];
            echo "<br /> cod fiscale " . $_POST["Paziente"];
            echo "<br /> tipo " . $_POST["tipoIntervento"];
            echo "<br /> data " . $_POST["Data"];

            $sql_insert = "insert into interventi (DATA_I, ID_D,"
                . " cod_FISC_P, ID_TIPO)"
                . " values ('" . $_POST["Data"] . "', " .  $_POST["Dentista"] 
                . " , '" . $_POST["Paziente"] . "', " . $_POST["tipoIntervento"]
                . ")";

            echo  "<br />query: " . $sql_insert;
            $res_insert=mysqli_query($conn,$sql_insert);
            
        }
    ?>
    ?>
    <h1>Inserimento Intervento</h1>
    <form method="post">
    <select name="Dentista">
        <option value=""></option>
        <?php
                foreach($tab_dentisti as $k=>$v) {
                    echo "<option value='$k'>$v</option>";
                }
            ?>
        
    </select>
    <select name="Paziente">
        <option value=""></option>
        <?php
                foreach($tab_pazienti as $k=>$v) {
                    echo "<option value='$k'>$v</option>";
                }
            ?>
    </select>
    <br/>
    Tipo Intervento: <input type="radio" name="tipoIntervento" value="1"/>Estrazione Dente del Giudizio <br/> 
    <input type="radio" name="tipoIntervento" value="2"/>Cura della Carie   
   <br/><input type="radio" name="tipoIntervento" value="3"/>Regolazione "EFFETTO VENTURI"
    <br/>Data Intervento :<input type="date"  name="Data" />
   
    <br/>
    <input type="reset" value="pulisci"/>
    <input type="submit" value="Salva Intervento"/>
    </form>
</body>
</html>