<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Studio Denstistico</title>
    <style>
        body {
            padding: 20px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16pt;
        }

        form {
            border: 1px solid black;
            padding: 10px;
            margin-bottom: 30px;
        }

        table {
            border: 1px solid black;
            border-collapse: collapse;
            width: 100%;

        }

        th:nth-child(odd) {
            border: 1px solid black;
            padding: 3px, 6px, 2px, 8px;
            text-align: left;
            background-color: red;
        }

        td:nth-child(odd){
            border: 1px solid black;
            background-color: cyan;
            text-align: center;
        }
        
        tr:nth-child(odd) {
            border:1 px solid black;
            background-color: greenyellow;
            

        }
    </style>
</head>
<?php
// leggo i valori scritti dall'utente
$id = "";
if (isset($_POST["id_dentista"])) {
    $tuttiInterventi = strtoupper($_POST["id_dentista"]);
}

$nome_paz = "";
if (isset($_POST["nome_paziente"])) {
    $nome_paz = $_POST["nome_paziente"];
}
$cognome_paz = "";
if (isset($_POST["cognome_paziente"])) {
    $cognome_paz = $_POST["cognome_paziente"];
}


// 1 - creo la connessione con il db
$host = "127.0.0.1";
$user = "root";
$password = "";
$db = "studio-dentistico";
$con = mysqli_connect($host, $user, $password, $db);
if (!$con) {
    die("Impossibile stabilire la connessione con il database");
} else {
    echo "Connessione OK";
}

// 2 - preparo la query
$sql_query = "select i.ID_INTERVENTO, i.DATA_I, d.NOME_D, d.COGNOME_D,"
    . " i.COD_FISC_P, p.NOME_P, p.COGNOME_P, i.ID_FATTURA, t.DESCRIZIONE"
    . " from interventi i"
    . " join dentisti d on i.ID_D = d.ID_DENTISTA"
    . " join pazienti p on i.COD_FISC_P = p.COD_FISCALE_PAZIENTE"
    . " join tipologia t on i.ID_TIPO= t.ID_TIPOLOGIA"
    . " where 1 = 1";


$sql_query2 = " select DATA_I"
    . " from interventi";
if ($id) {
    $sql_query2 .= " where ID_D= '" . $id . "'";
}



if ($nome_paz) {
    $sql_query .= " and p.NOME_P like '" . $nome_paz . "%'";
}
if ($cognome_paz) {
    $sql_query .= " and p.COGNOME_P like '" . $cognome_paz . "%'";
}

// 3 - eseguo il comando
$res = mysqli_query($con, $sql_query);
$res2 = mysqli_query($con, $sql_query2);
/*
    // 4 - prendo i risultati della query
    while($riga = mysqli_fetch_assoc($res)) {
        echo "<br />" . $riga["cognome_paziente"];
    }

    // 5 - chiudo la connessione
    mysqli_close($con);
    */
?>

<body>
    <h1><p style="color:red;">Ricerca Interventi</p></h1>
    Inserisci i dati del paziente e premi il tasto ricerca.
    <form method="post">
        <br />Nome: <input type="text" maxlength="200" placeholder="Nome Paziente" name="nome_paziente" value="<?= $nome_paz ?>" />
        <br />Cognome: <input type="text" maxlength="200" placeholder="Cognome Paziente" name="cognome_paziente" value="<?= $cognome_paz ?>" />
        <br /><input type="submit" value="Ricerca" />

        <br />

    </form>
    Ricerca per <?php echo $nome_paz; ?> <?= $cognome_paz ?>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Data Intervento</th>
                <th>Dentista</th>
                <th>Paziente</th>
                <th>Tipo Intervento</th>
                <th>Fattura</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // 4 - prendo i risultati della query
            while ($riga = mysqli_fetch_assoc($res)) {
                echo "<tr>";
                echo "<td>" . $riga["ID_INTERVENTO"] . "</td>";
                echo "<td>" . $riga["DATA_I"] . "</td>";
                echo "<td>" . $riga["COGNOME_D"] . " " . $riga["NOME_D"] . "</td>";
                echo "<td>" . $riga["COGNOME_P"] . " " . $riga["NOME_P"] . "</td>";
                echo "<td>" . $riga["DESCRIZIONE"] . "</td>";
                echo "<td>" . $riga["ID_FATTURA"] . "</td>";
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>
    <br />
    <form method="post">
        <input type="text" maxlength="200" placeholder="id" name="id_dentista" value="<?= $id ?>" />
        <input type="radio" name="tutti_interventi" value="IN" />Tutti gli Interventi
        <br /><input type="submit" value="Avvia Ricerca" />

    </form>
    tutti gli interventi dei dentisti per data: <?php $id ?>
    <table>
        <thead>
            <tr>

                <th>Data Intervento</th>

            </tr>
        </thead>
        <tbody>

            <?php
            // 4 - prendo i risultati della query
            while ($riga = mysqli_fetch_assoc($res2)) {
                echo "<tr>";
                echo "<td>" . $riga["DATA_I"] . "</td>";
                echo "</tr>";
            }
            mysqli_close($con);
            ?>

        </tbody>
    </table>
    // 5 - chiudo la connessione
    mysqli_close($con);
    ?>



</body>

</html>