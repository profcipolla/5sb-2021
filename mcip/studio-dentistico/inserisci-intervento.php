<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inserimento Intervento</title>
</head>

<body>
    <?php
        $host = "127.0.0.1";
        $user = "root";
        $pwd = "nopassword";
        $db = "studio";

        $conn = mysqli_connect($host, $user, $pwd, $db);
        if (!$conn) {
            die("Errore durante la connessione " . mysqli_connect_error());
        }
        // echo "Connessione stabilita correttamente";

        $sql_dentisti = "select id_dentista, cognome, nome"
            . " from dentista"
            . " order by cognome, nome";
        
        $res_dentisti = mysqli_query($conn, $sql_dentisti);
        if (!$res_dentisti) {
            die("Errore durante l'esecuzione della query " . $sql_dentisti);
        }
        // echo "Query eseguita correttamente";
        $tab_dentisti = [];
        while ($riga_dentista = mysqli_fetch_assoc($res_dentisti)) {
            $tab_dentisti[$riga_dentista["id_dentista"]] = $riga_dentista["cognome"] . " ". $riga_dentista["nome"];
        }

        $sql_pazienti = "select codice_fiscale, cognome, nome"
            . " from paziente"
            . " order by cognome, nome";
        $res_pazienti = mysqli_query($conn, $sql_pazienti);
        if (!$res_pazienti) {
            die("Errore durante l'esecuzione della query " . $sql_pazienti);
        }
        $tab_pazienti = [];
        while($riga_paziente = mysqli_fetch_assoc($res_pazienti)) {
            $tab_pazienti[$riga_paziente["codice_fiscale"]] = $riga_paziente["cognome"] 
                . " " 
                . $riga_paziente["nome"] 
                . " - " 
                . $riga_paziente["codice_fiscale"];
        }

        // var_dump($tab_dentisti);
        // var_dump($tab_pazienti);

        echo "Metodo invocato " . $_SERVER["REQUEST_METHOD"];
        if ("POST" == $_SERVER["REQUEST_METHOD"]) {
            echo "<br />Devo salvare i dati";
            echo "<br /> id dentista " . $_POST["dentista"];
            echo "<br /> cod fiscale " . $_POST["paziente"];
            echo "<br /> tipo " . $_POST["tipoIntervento"];
            echo "<br /> data " . $_POST["data"];

            $sql_insert = "insert into intervento (data_intervento, id_dentista,"
                . " codice_fiscale, id_tipologia)"
                . " values ('" . $_POST["data"] . "', " .  $_POST["dentista"] 
                . " , '" . $_POST["paziente"] . "', " . $_POST["tipoIntervento"]
                . ")";

            echo  "<br />query: " . $sql_insert;
        }
    ?>

    <h1>Inserimento intervento</h1>
    <form method="post">
        Dentista: <select name="dentista">
            <option value=""></option>
            <?php
                foreach($tab_dentisti as $k=>$v) {
                    echo "<option value='$k'>$v</option>";
                }
            ?>
        </select>

        <br />Paziente: <select name="paziente">
            <option value=""></option>
            <?php
                foreach($tab_pazienti as $k=>$v) {
                    echo "<option value='$k'>$v</option>";
                }
            ?>
        </select>

        <br />Tipo Intervento: 
        <input type="radio" name="tipoIntervento" value="1" />Estrazione Dente Giudizio
        <input type="radio" name="tipoIntervento" value="2" />Cura carie

        <br />Data Intervento: <input type="date" name="data" />

        <br>
        <input type="reset" value="Pulisci" />
        <input type="submit" value="Salva Intervento" />
    </form>
</body>

</html>