<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ricerca Interventi</title>
    <style>
        body {
            padding: 20px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16pt;
        }

        form {
            border: 1px solid black;
            padding: 10px;
            margin-bottom: 30px;
        }

        table {
            border: 1px solid gray;
            border-collapse: collapse;
            width: 100%;
        }

        th {
            border: 1px solid gray;
            padding: 3px, 6px, 2px, 8px;
            text-align: left;
        }

        td {
            border: 1px solid gray;
        }
    </style>
</head>
<?php
    // leggo i valori scritti dall'utente
    $nome_paz = "";
    if (isset($_POST["nome_paziente"])) {
        $nome_paz = $_POST["nome_paziente"];
    }
    $cognome_paz = "";
    if (isset($_POST["cognome_paziente"])) {
        $cognome_paz = $_POST["cognome_paziente"];
    }


    // 1 - creo la connessione con il db
    $con = mysqli_connect("127.0.0.1", "root", "nopassword", "studio");
    if (!$con) {
        die("Impossibile stabilire la connessione con il database");
    } else {
        echo "Connessione OK";
    }

    // 2 - preparo la query
    $sql_query = "select i.id_intervento, i.data_intervento, d.nome nome_dentista, d.cognome cognome_dentista," 
	    . " i.codice_fiscale, p.nome nome_paziente, p.cognome cognome_paziente, i.id_fattura, t.descrizione"
        . " from intervento i"
	    . " join dentista d on i.id_dentista = d.id_dentista"
        . " join paziente p on i.codice_fiscale = p.codice_fiscale"
        . " join tipologia_intervento t on i.id_tipologia = t.id_tipologia"
        . " where 1 = 1";
    
    if ($nome_paz) {
        $sql_query .= " and p.nome like '" . $nome_paz . "%'";
    }
    if ($cognome_paz) {
        $sql_query .= " and p.cognome like '" . $cognome_paz . "%'";
    }

    // 3 - eseguo il comando
    $res = mysqli_query($con, $sql_query);
/*
    // 4 - prendo i risultati della query
    while($riga = mysqli_fetch_assoc($res)) {
        echo "<br />" . $riga["cognome_paziente"];
    }

    // 5 - chiudo la connessione
    mysqli_close($con);
    */
?>
<body>
    <h1>Ricerca Interventi</h1>
    Inserisci i dati del paziente e premi il tasto ricerca.
    <form method="post">
        <br />Nome: <input type="text" maxlength="200" placeholder="Nome Paziente" name="nome_paziente" value="<?=$nome_paz?>" />
        <br />Cognome: <input type="text" maxlength="200" placeholder="Cognome Paziente" name="cognome_paziente" value="<?=$cognome_paz?>" />
        <br /><input type="submit" value="Ricerca" />
    </form>

    Ricerca per <?php echo $nome_paz; ?> <?=$cognome_paz ?>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Data Intervento</th>
                <th>Dentista</th>
                <th>Paziente</th>
                <th>Tipo Intervento</th>
                <th>Fattura</th>
            </tr>
        </thead>
        <tbody>
        <?php
            // 4 - prendo i risultati della query
            while($riga = mysqli_fetch_assoc($res)) {
                echo "<tr>";
                echo "<td>" . $riga["id_intervento"] . "</td>";
                echo "<td>" . $riga["data_intervento"] . "</td>";
                echo "<td>" . $riga["cognome_dentista"] . " " . $riga["nome_dentista"] ."</td>";
                echo "<td>" . $riga["cognome_paziente"] . " " . $riga["nome_paziente"] . "</td>";
                echo "<td>" . $riga["descrizione"] . "</td>";
                echo "<td>" . $riga["id_fattura"] . "</td>";
                echo "</tr>";
            }

            // 5 - chiudo la connessione
            mysqli_close($con);
        ?>
        </tbody>
    </table>

</body>

</html>