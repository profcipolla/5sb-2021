import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerLettereDoppie {
    private static final int PORT = 9876;
    
    public static void main(String[] a) {
        try {
            ServerSocket mioSocket = new ServerSocket(PORT);
            while(true) {
                System.out.println("Sono in ascolto sulla porta " + PORT);
                Socket client = mioSocket.accept();
                System.out.println("... connessione accettata");
                
                InputStreamReader isr = new InputStreamReader(client.getInputStream());
                // canale di input, da cui leggiamo i dati che il
                // client ci invia
                BufferedReader in = new BufferedReader(isr);
                
                // canale di output, invio i dai al client che si
                // è collegato
                PrintWriter out = new PrintWriter (client.getOutputStream());
            
                String messaggioRicevuto = in.readLine();
                
                String risposta = "";
                // raddoppio le lettere
                int len = messaggioRicevuto.length();
                for (int i = 0; i < len; i++) {
                    risposta += String.valueOf(messaggioRicevuto.charAt(i)) + String.valueOf(messaggioRicevuto.charAt(i));
                }
                
                out.write(risposta + "\n");
                System.out.println("... risposta inviata: " + risposta);
                out.flush();
                System.out.println("... chiudo connessione");
                out.close();
            } // while
        } catch (IOException e) {
            System.err.println("Errore durante la creazione del socket");
            System.err.println(e.getMessage());
        }
    }
}
