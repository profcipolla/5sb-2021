import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class ClientGui extends JFrame implements ActionListener {
    private static int PORT = 9876;
    private static int WIDTH = 800;
    private static int HEIGHT = 300;
    private static String SRV_BENVENUTO = "Per favore scrivi qualcosa...";
    private static String SRV_CIAO = "CIAO";

    private JLabel lblMessaggio = new JLabel("Messaggio");
    private JTextField tfMessaggio = new JTextField();
    private JButton jbInvia = new JButton("Invia");
    private JLabel lblRisposta = new JLabel();

    public ClientGui() {
        setTitle("Client Socket");
        setBounds(20, 20, WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        LayoutManager mioLayout = new GridLayout(0, 1, 10, 10);
        setLayout(mioLayout);

        jbInvia.addActionListener(this);
        tfMessaggio.setPreferredSize(new Dimension(600, 20));
        JPanel pnlMessaggio = new JPanel();
        pnlMessaggio.setLayout(new FlowLayout());
        pnlMessaggio.add(lblMessaggio);
        pnlMessaggio.add(tfMessaggio);
        add(pnlMessaggio);
        add(jbInvia);
        add(lblRisposta);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        lblRisposta.setText("");
        // System.out.println("Hai premuto sul pulsante INVIA");
        String messaggio = tfMessaggio.getText();
        // if () se il messaggio è stato inserito, qui sapete voi cosa fare

        try {
            Socket clientSocket = new Socket("localhost", PORT);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream());
            
            int passo = 0;
            String strIn = in.readLine();
            while(null != strIn) {
                System.out.print(strIn);
                // se ho ricevuto il messaggio d'invito del server...
                if (SRV_BENVENUTO.equals(strIn)) {
                    lblRisposta.setText(strIn + "\n");
                    // ... invio la stringa dell'utente
                    System.out.println("Invio il messaggio: " + messaggio);
                    out.write(messaggio + "\n");
                    out.flush();
                    passo++;
                } else if (1 == passo) {
                    lblRisposta.setText(lblRisposta.getText() +  strIn + "\n");
                    out.write(SRV_CIAO + "\n");
                    out.flush();
                    passo++;
                } else if (2 == passo) {
                    lblRisposta.setText(lblRisposta.getText() +  strIn + "\n");
                    passo++;
                    clientSocket.close();
                }
                strIn = in.readLine();
            }

        } catch (IOException exc) {
            System.out.println("ERRORE: " + exc.getMessage());
        }
    }
}
