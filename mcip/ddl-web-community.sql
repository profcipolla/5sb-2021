create table categorie(
    idCategoria int not null auto_increment primary key,
    descrizione varchar(200)
);

create table province (
	sigla char(2) not null primary key,
    nome varchar(100) not null
);

create table citta (
	codiceIstat char(4) not null primary key,
    nome varchar(100) not null,
    siglaFk char(2) not null
);

alter table citta
add constraint fk_citta_2_province
foreign key (siglaFk)
references province(sigla);

create table artisti(
	codiceFiscale char(16) not null primary key,
    natoIl date,
    nome varchar(100) not null,
    cognome varchar(200) not null
);

create table post(
	idPost int not null auto_increment primary key,
    commento varchar(200) not null,
    voto int not null,
    nickNameFk varchar(100) not null,
    idEventoFk int not null
);

alter table post
add constraint fk_post_2_eventi
foreign key (idEventoFk)
references eventi(idEvento);

alter table post
add constraint fk_post_2_membri
foreign key (nickNameFk)
references membri(nickName);

create table membri(
	nickName varchar(100) not null primary key,
    nome varchar(100) not null,
    cognome varchar(200) not null,
    email varchar(300) not null,
    password varchar(400) not null,
    siglaFk char(2) not null
);

alter table membri
add constraint fk_membri_2_province
foreign key (siglaFk)
references province(sigla);

create table eventi(
	idEvento int not null auto_increment primary key,
    dataEvento date not null,
    via varchar(200) not null,
    codiceIstatFk char(4) not null,
    titolo varchar(200) not null,
	nickName varchar(100) not null,
    idCategoriaFk int not null
);

create table sceglie(
	nickName varchar(100) not null,
    idCategoria int not null,
	primary key (nickName, idCategoria)
);

alter table sceglie
add constraint fk_sceglie_2_membri
foreign key (nickName)
references membri(nickName);

alter table sceglie
add constraint fk_sceglie_2_categorie
foreign key (idCategoria)
references categorie(idCategoria);


create table sono_coinvolti(
    idEvento int not null,
	codiceFiscale char(16) not null,

	primary key (idEvento, codiceFiscale)
);

alter table sono_coinvolti
add constraint fk_sono_coinvolti_2_eventi
foreign key (idEvento)
references eventi(idEvento);

alter table sono_coinvolti
add constraint fk_sono_coinvolti_2_artisti
foreign key (codiceFiscale)
references artisti(codiceFiscale);

	