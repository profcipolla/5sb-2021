package it.armellini.vsb.servletwf.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Salve
 */
@WebServlet(description = "Invia un saluto", urlPatterns = { "/salve" })
public class Salve extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String htmlBenvenuto = 
			"  <html>"
			+ "<head>"
			+ " <title>Prova Servlet</title>"
			+ "</head>"
			+ "<body>"
			+ " <h1>Saluto</h1>"
			+ " SALVE %s %s!"
			+ " %s"
			+ "</body>"
			+ "</html>";
    private static final String jsBenventuo =
    		"   <script>"
			+ " 	alert('Attenzione: messaggio da Wildfly');"
			+ " </script>";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Salve() {
        super();
        System.err.println("Viene invocato il costruttore della classe Salve");
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.err.println("Viene invocato il metodo init");
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	/*
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
*/
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		// parametro nome presente nella richiesta del browser
		String pNome = req.getParameter("nome");
		String pCognome = req.getParameter("cognome");
		
		String pFinestra = req.getParameter("finestra");
		String terzo = "";
		if ("s".equalsIgnoreCase(pFinestra)) {
			terzo = jsBenventuo;
		}
		
		PrintWriter out = resp.getWriter();
		out.write(String.format(htmlBenvenuto, pCognome, pNome, terzo));
		out.flush();
		out.close();

		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
