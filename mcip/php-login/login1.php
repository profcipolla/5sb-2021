<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LOGIN</title>
</head>
<?php
    session_start();

    $host = "127.0.0.1";
    $userDb = "root";
    $pwdDb = "nopassword";
    $db = "studio";


    $user = $_POST["username"];
    $pwd = $_POST["password"];
    $messaggio = '';
    $messaggio_sessione = $_SESSION["errore"];
    $_SESSION["errore"] = '';

    $_SESSION['user_login'] = '';
    $_SESSION['user_role'] = '';

    if ('POST' == $_SERVER['REQUEST_METHOD']) {
        $messaggio = "Errore non previsto dall'applicazione";

        $conn = mysqli_connect($host, $userDb, $pwdDb, $db);
        if (!$conn) {
            die("Errore durante la connessione al databse " . mysqli_connect_error());
        }

        $query = "select u.ruolo"
            . " from utente u"
            . " where u.username = '" . mysqli_real_escape_string($conn, $user) . "'"
            . " and u.pwd = '" . mysqli_real_escape_string($conn, md5($pwd)) . "'";

        echo "QERY: " . $query;

        $res = mysqli_query($conn, $query);
        if (!$res) {
            die("Errore durante l'esecuzione della query " . $query);
        }

        $row = mysqli_fetch_assoc($res);
        if ($row) {
            $messaggio = "Login effettuato con successo";
            $ruolo = $row['ruolo'];
            $_SESSION['user_login'] = $user;
            $_SESSION['user_role'] = $ruolo;
        } else {
            $messaggio = "Nome utente o password errati";
            $ruolo = '';
        }
        mysqli_close($conn);


        // è stato eseguito correttamente il login, mi dirotta alla pagina sicura
        if ($ruolo) {
            header("location: sicura1.php");
        }
    }

?>

<body>
    <h1>PAGINA LOGIN</h1>
    <form method="post">
        Username: <input type="text" name="username" maxlength="15" value="<?= $user ?>" />
        <br />Password <input type="password" name="password" maxlength="20" />
        <br /><input type="submit" value="Accedi" />
    </form>
    <?= $messaggio ?>
    <br /><h2><?= $messaggio_sessione ?></h2>
</body>

</html>