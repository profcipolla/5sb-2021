<?php
    session_start();
    $userLogin = $_SESSION['user_login'];
    $userRole = $_SESSION['user_role'];

    // se l'utente non ha fatto il login correttamente, lo rimando alla pagina di login
    if (!$userLogin) {
        $_SESSION["errore"] = "Per favore effettua il login";
        header("location: login1.php");
    } else {
        $_SESSION["errore"] = "";
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SICURA</title>
</head>

<body>
    <h1>PAGINA RISERVATA</h1>
    Se riesci a vedere questa pagina vuol dire che hai eseguito il login
    <br />Benvenuto utente: <?= $userLogin ?>
    <br />ruolo: <?= $userRole ?>
</body>

</html>