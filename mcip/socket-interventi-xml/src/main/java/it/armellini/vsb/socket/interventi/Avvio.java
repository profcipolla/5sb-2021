package it.armellini.vsb.socket.interventi;

public class Avvio {

    public static void main(String[] args) {
        // il socket si pone in ascolto
        SocketInterventi sockInterventi = new SocketInterventi();
        sockInterventi.start();
        
        // InterventiRepository iRepo = new InterventiRepository();
        // String xml = iRepo.getXmlInterventi("R");
        // System.out.println(xml);
    }

}
