package it.armellini.vsb.socket.interventi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InterventiRepository {
    private String host = "127.0.0.1";
    private String port = "3306";
    private String db = "studio";
    private String user = "root";
    private String pwd = "nopassword";

    private final String ELEMENTO_INTERVENTO = "<intervento id=\"%s\">"
            + "\n <dentista>%s</dentista>"
            + "\n <paziente>%s</paziente>"
            + "\n <data_intervento>%s</data_intervento>"
            + "\n</intervento>";
    
    public String getXmlInterventi(String inizialeCognomePaziente) {
        String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "\n<interventi>"
                + "\n%s"
                + "\n</interventi>";
        
        // 1) carico il driver
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException exc) {
            System.err.println("Errore durante il caricamento del driver di mariadb");
            System.err.println(exc.getMessage());
            return xmlString;
        }

        // 2) connessione
        Connection connection = null;
        try {
            String strConn = String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, db, user, pwd);
            connection = DriverManager.getConnection(strConn);
        } catch (SQLException exc) {
            System.err.println("Errore durante la connessione al database studio");
            System.err.println(exc.getMessage());
            return xmlString;
        }

        try {
            // 3) statement
            Statement stm = connection.createStatement();

            // @formatter:off
            String query = "select i.id_intervento,"
                    + " concat(d.cognome, ' ', d.nome) ndentista,"
                    + " concat(p.cognome, ' ', p.nome) npaziente, i.data_intervento"
                    + " from intervento i"
                    + " join paziente p on p.codice_fiscale = i.codice_fiscale"
                    + " join dentista d on d.id_dentista = i.id_dentista"
                    // qui condizione sul cognome del paziente
                    + " where p.cognome like '" + inizialeCognomePaziente + "%'"
                    + " order by i.data_intervento desc";
            // @formatter:on

            System.out.println("Eseguo la query: " + query);

            // 4) esecuzione statement
            ResultSet res = stm.executeQuery(query);

            // 5) navigo i risultati
            StringBuilder sb = new StringBuilder();
            while (res.next()) {
                String out = String.format(this.ELEMENTO_INTERVENTO, 
                        res.getInt("id_intervento"),
                        res.getString("ndentista"),
                        res.getString("npaziente"),
                        res.getString("data_intervento"));
                sb.append(out);
            }

            xmlString = String.format( xmlString, sb.toString());
            // 6) chiudo la connessione
            connection.close();

        } catch (SQLException e) {
            System.err.println("Errore: " + e.getMessage());
        }
        
        return xmlString;
    }
}
