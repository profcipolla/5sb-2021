package it.armellini.vsb.socket.interventi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketInterventi {
    private static int PORT = 9876;

    public void start() {
        try {
            ServerSocket echoSocket = new ServerSocket(PORT);
            do {
                System.out.println("Sono in ascolto sulla porta " + PORT);
                Socket clientSocket = echoSocket.accept();

                BufferedReader inputBr = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintWriter outPw = new PrintWriter(clientSocket.getOutputStream());

                outPw.write("Per favore inserisci l'iniziale del cognome...\n");
                outPw.flush();

                String inizialeCognome = inputBr.readLine();
                InterventiRepository interventiRepository = new InterventiRepository();
                String outXml = interventiRepository.getXmlInterventi(inizialeCognome);
                
                outPw.write(outXml + "\n");
                outPw.flush();
                clientSocket.close();
            } while (true);
        } catch (IOException e) {
            System.out.println("Errore durante l'apertura del socket: " + e.getMessage());
        }

    }
}
