<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Esempio pagina PHP</title>
</head>
<body>
    Questo viene da HTML
    <br />
    <?php
        $a = 1;
        $b = 0;

        $c = $a + $b;

        echo "<br />";
        // mostra il risultato
        echo "Il risultato di $a + $b è $c"; // anche questo è commento

        /*
        questo è un commento multilinea
        Anche questa linea fa parte del commento
        */
        echo '<br />';
        echo 'Il risultato di $a + $b è $c';

        echo "<br />";
        if ($c > 20) {
            echo "Il risultato è maggiore di 20";
        } else {
            echo "Il risultato è minore o uguale a 20";
        }

        echo "<br />";
        if ($c < 10) {
            echo "Tra 0 e 9";
        } else if($c < 20) {
            echo "Tra 10 e 19";
        } else if ($c < 30) {
            echo "Tra 20 e 29";
        } else if ($c < 40) {
            echo "Tra 30 e 39";
        }

        // $i = $i + 1
        // $i += 1
        for ($i = 1; $i <= 10; $i += 2) {
            $t = $i * 2;
            echo "<br /> $t";
        }
    ?>
</body>
</html>