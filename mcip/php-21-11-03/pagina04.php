<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tavola Pitagorica</title>
</head>
<body>
    Questo programma scrive la tavola pitagorica dei numeri da 1 a 10
    <br />
    Per casa: i numeri della tavola vanno inseriti in una tabella HTML,
    bordo esterno rosso, celle con bordo grigio, come pagina01.html

    <?php
        for ($n = 1; $n <= 10; $n++) {
            for ($c = 1; $c <=10; $c++) {
                $r = $c * $n;
                echo "  $r";
            }
            echo "<br />";
        }
    ?>
</body>
</html>