import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ElencoInterventi {
    
    
    public static void main(String[] args) {
        String host = "127.0.0.1";
        String port = "3306";
        String db = "studio";
        String user = "root";
        String pwd = "nopassword";
        
        // 1) carico il driver
            
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException exc) {
            System.err.println("Errore durante il caricamento del driver di mariadb");
            System.err.println(exc.getMessage());
            return;
        }
        
        // 2) connessione
        Connection connection = null;
        try {
            String strConn = String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, db, user, pwd);
            connection = DriverManager.getConnection(strConn);
        } catch (SQLException exc) {
            System.err.println("Errore durante la connessione al database studio");
            System.err.println(exc.getMessage());
            return;
        }
        
        try {
            // 3) statement
            Statement stm = connection.createStatement();
        
            String query = "select concat(d.cognome, ' ', d.nome) ndentista, concat(p.cognome, ' ', p.nome) npaziente, i.data_intervento"
                    + " from intervento i"
                    + " join paziente p on p.codice_fiscale = i.codice_fiscale"
                    + " join dentista d on d.id_dentista = i.id_dentista"
                    + " order by i.data_intervento";
            
            // 4) esecuzione statement
            ResultSet res = stm.executeQuery(query);
            
            // 5) navigo i risultati
            while (res.next()) {
               System.out.println(res.getString("ndentista"));
            }
            
            // 6) chiudo la connessione
            connection.close();
            
        } catch (SQLException e) {
        }
        

        
        
        
        
        
    }
}
