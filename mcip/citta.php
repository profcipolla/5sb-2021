<html>

<head>
    <title>Elenco Citt&agrave;</title>
    <style>
        .tb_comuni {
            border: 1px solid gray;
            border-collapse: collapse;
            width: 90%;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14pt;
        }

        .tb_comuni td {
            border: 1px solid darkgray;
            padding: 4px 8px;
        }

        .tb_comuni th {
            border: 1px solid darkgray;
            padding: 6px 8px;
            font-size: 15pt;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <h1>Elenco dei comuni della provincia di Roma</h1>
    <?php
    $filtroProvincia = "";
    if (isset($_GET["filtro_provincia"])) {
        $filtroProvincia = strtoupper($_GET["filtro_provincia"]);
    }

    $orderBy = "";
    $orderByDesc = "";
    if (isset($_GET["orderBy"])) {
        switch (strtoupper($_GET["orderBy"])) {
            case "CI":
                $orderBy = " order by id";
                $orderByDesc = "Codice Istat";
                break;
            case "NM":
                $orderBy = " order by nome";
                $orderByDesc = "Nome Città";
                break;
            case "CP":
                $orderBy = " order by cap";
                $orderByDesc = "CAP";
                break;
            case "NA":
                $orderBy = " order by abitanti";
                $orderByDesc = "Numero Abitanti";
                break;
        }
    }

    $host = "127.0.0.1";
    $user = "root";
    $password = "nopassword";
    $db = "periodici";
    // echo "<br />Tentativo connessione";
    $conn = mysqli_connect($host, $user, $password, $db);
    if (!$conn) {
        echo "<br />Connessione fallita";
        die("Impossibile collegarsi al database. " . mysqli_connect_error());
    }
    ?>

    <form method="get">
        Filtro sigla provincia:
        <select name="filtro_provincia">
            <option value=''></option>
            <?php
            $query = "select sigla_pr, nome"
                . " from province"
                . " order by nome";
            $resProvince = mysqli_query($conn, $query);
            if (!$resProvince) {
                die("Impossibile eseguire la query " . $resProvince);
            }

            while ($unaProvincia = mysqli_fetch_assoc($resProvince)) {
                if ($filtroProvincia == $unaProvincia["sigla_pr"]) {
                    echo "<option selected='true' value='" . $unaProvincia["sigla_pr"] . "'>" . $unaProvincia["nome"] . "</option>";
                } else {
                    echo "<option value='" . $unaProvincia["sigla_pr"] . "'>" . $unaProvincia["nome"] . "</option>";
                }
            }
            ?>
        </select>
        <br />
        Ordina per:
        <input type="radio" name="orderBy" value="CI" /> Codice Istat
        &nbsp;&nbsp;<input type="radio" name="orderBy" value="NM" /> Nome Citt&agrave;
        &nbsp;&nbsp;<input type="radio" name="orderBy" value="CP"> CAP
        &nbsp;&nbsp;<input type="radio" name="orderBy" value="NA"> Numero Abitanti
        <br />
        <input type="checkbox" value="DESC" name="direzione" /> Ordinamento decrescente
        <br />
        <input type="submit" value="Ricerca" />
    </form>
    <table class="tb_comuni">
        <caption>Elenco dei comuni
            <?php
            if ($filtroProvincia) {
                echo " della provincia $filtroProvincia";
            }

            if ($orderBy) {
                echo ", ordinato per $orderByDesc";
            }
            ?>

        </caption>
        <thead>
            <tr>
                <th>Progressivo</th>
                <th>Codice Istat</th>
                <th>Nome</th>
                <th>CAP</th>
                <th>Provincia</th>
                <th>Num. Abitanti</th>
            </tr>
        </thead>
        <tbody>
            <?php

            // echo "<br />Connessione riuscita";

            $query = "select c.id, c.nome, c.cap, c.sigla_pr, c.abitanti, p.nome provincia"
                . " from citta c"
                . "  join province p on c.sigla_pr = p.sigla_pr";

            if ($filtroProvincia) {
                $query .= " where c.sigla_pr = '" . $filtroProvincia . "'";
            }

            if ($orderBy) {
                $query .= $orderBy;
            }

            $resultset = mysqli_query($conn, $query);
            if (!$resultset) {
                die("Errore durante l'esecuzione della query: " . $query);
            }
            // echo "<br />Query eseguita correttamente";

            $num_row = 0;
            while ($row = mysqli_fetch_assoc($resultset)) {
                ++$num_row;
                $codIstat = $row["id"];
                $nomeCitta = $row["nome"];
                $cap = str_pad($row["cap"], 5, "0", STR_PAD_LEFT);
                $provincia = $row["provincia"];
                $abitanti = $row["abitanti"];
                echo "<tr>";
                echo "<td>$num_row</td><td>$codIstat</td><td>$nomeCitta</td><td>$cap</td><td>$provincia</td><td>$abitanti</td>";
                echo "</tr>";
            }

            mysqli_close($conn);
            ?>
        </tbody>
    </table>
</body>

</html>