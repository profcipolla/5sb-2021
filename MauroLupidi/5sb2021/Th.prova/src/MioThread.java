import java.util.Random;

public class MioThread extends Thread {

	private Integer conta;
	private String nome;
	private Integer ritardo;
	
	private RisorsaCondivisa risorsaCondivisa;
	
	
	public MioThread(String nome, Integer ritardo, RisorsaCondivisa risorsaCondivisa) {
	this.nome=nome;
	this.conta=0;
	this.ritardo=ritardo;
	this.risorsaCondivisa=risorsaCondivisa;
	
	
}
	public void run() {//qui il codice viene avviato quando si invoca star() sull'oggetto instaziata dalla classe MioThread
	Random rnd= new Random();
	Integer conta=0;
	
		while (++conta <=100) {
		try {
			Thread.sleep(rnd.nextInt(1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Integer toPush=rnd.nextInt();
		this.risorsaCondivisa.push(toPush);
		System.out.println(this.nome + " - push:" + this.conta);
	}
	}
	
}
