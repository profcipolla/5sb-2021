package cicli;

public class Avvio {

	public static void main(String[] args) {
		for (int i = 1; i <= 10; i++) {
			// ciclo basato sulle numerazioni e servo prettamente per contare
			System.out.println(i * 3); // stampa la tabellina del 3 � solo un esempio
		}

		int i = 1;
		while (i <= 10) { // ripete se la condizione � vera
			System.out.println(i * 3);
			i = +3;

		}

	}
}
