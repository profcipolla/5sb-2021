package it.armellini.ivsb.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet(urlPatterns = "/test")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private static final String HTML_RISPOSTA = "<html><head><title>Risposta</title></head><body>Salve %NOME%</body></html>";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        System.out.println("Wildfly mi ha costruito");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		out.print("<html>");
		out.print("<head>");
		out.print("<title>Mio Sito</title>");
		out.print("</head>");
		out.print("<body>");
		out.print("<h1>Pagina HTML generata da Java</h1>");
		out.print("<form method='post' action='/miosito-1/test'>");
		out.print("Nome: <input type='text' name='mionome' />");
		out.print("<br /> <input type='submit' value='Saluto' />");
		out.print("</form>");
		out.print("</body>");
		out.print("</html>");
		
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		String inputUtente = req.getParameter("mionome");
		out.print(HTML_RISPOSTA.replace("%NOME%", inputUtente));
		
		out.flush();
		out.close();
	}

}
