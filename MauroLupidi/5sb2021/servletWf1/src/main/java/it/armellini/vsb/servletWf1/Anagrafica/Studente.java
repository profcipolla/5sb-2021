package it.armellini.vsb.servletWf1.Anagrafica;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Studente {
	private String nome;
	private String cognome;
	private int matricola;
	
	
	public String toString() {
		return String.format("%s %s, %d", this.cognome, this.nome, this.matricola);
	}
	
	

}
