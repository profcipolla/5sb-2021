package it.armellini.vsb.servletWf1.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Salve
 */
@WebServlet(description = "invia un saluto", urlPatterns = { "/Salve" })
public class Salve extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String htmlBenvenuto=
			"<html>"
			+ "<head>"
			+"<title>Prova Servlet</title>"
			+"</head>"
			+"<body>"
			+"<h1>Saluto</h1>"
			+"SALVE %s %s"
			+"<script>"
			+ " alert('Attenzione: messaggio da Wildfly');"
			+"</script>"
			+"</body>"
			+"</html>";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Salve() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.err.println("viene invocato il metodoto INIT");
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//parametro nome presente nella richiesta del browser
		
		String pNome =request.getParameter("nome");
		String pCognome=request.getParameter("cognome");
		
		String pFinestra=request.getParameter("finestra");
		
		PrintWriter out=response.getWriter();
		out.write(String.format(htmlBenvenuto, pNome, pCognome));
		out.flush();
		out.close();
		
		
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
