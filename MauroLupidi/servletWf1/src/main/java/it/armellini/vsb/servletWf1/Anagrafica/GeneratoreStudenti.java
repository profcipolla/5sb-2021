package it.armellini.vsb.servletWf1.Anagrafica;

import java.util.ArrayList;
import java.util.List;

public class GeneratoreStudenti {

	public List<Studente> genera(){
		
		List<Studente> resultList=new ArrayList<>();
		
		for(int i=0 ;i<20; i++) {
			Studente s= new Studente("Nome " +i, "Cognome "+i, i);
			resultList.add(s);
		}
		return resultList;
	}
}
