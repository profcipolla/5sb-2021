package it.armellini.vsb.servletWf1.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.armellini.vsb.servletWf1.Anagrafica.GeneratoreStudenti;
import it.armellini.vsb.servletWf1.Anagrafica.Studente;

/**
 * Servlet implementation class StudenteServlet
 */
@WebServlet(urlPatterns="/studenti")
public class StudenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Gson gson= new Gson();
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudenteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		GeneratoreStudenti gs= new GeneratoreStudenti();
		List<Studente> listaStudenti =gs.genera();
		PrintWriter out=response.getWriter();
		// qui serializzazione
		
		out.write(this.gson.toJson(listaStudenti));
		out.flush();
		out.close();
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//iceve un json contenente un array di studenti
	//scrivo i dati sulla console
		
		String tipoDati = request.getParameter("tipoDati");
		
		
		String inputJson="";
		try(BufferedReader bodyReader =request.getReader()){
			String riga=bodyReader.readLine();
			while (null !=riga) {
				inputJson +=riga + '\n';
				riga=bodyReader.readLine();
			}
			
		}
		Type type=new TypeToken<List<Studente>>(){}.getType();
		List<Studente> listaStudenti=(List<Studente>)this.gson.fromJson(inputJson, Studente.class);
		for(Studente unoStudente: listaStudenti){
			System.out.println(unoStudente);
			
		}
		
		PrintWriter out=response.getWriter();
	
		
		out.write("Ho ricevuto 10 dati di "+listaStudenti.size()+""+tipoDati);
		out.flush();
		out.close();
		
	}

}
