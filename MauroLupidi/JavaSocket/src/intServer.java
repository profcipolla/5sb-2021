import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class intServer {

	public static void main(String[] args) {
		// creare il server

		try {
			ServerSocket ss = new ServerSocket(9999);

			while (true) {
				// accettare connessione dal cliente
				Socket s = ss.accept(); // <--------qui si mette in ascolto con la parole (accept)
				// leggere i dati passati
				System.out.println("Server: " + s.getInputStream().read());
				// tornare all'inzio per accettare una nuova connessione
				s.close();
			}

		} catch (IOException e) {

			e.printStackTrace(); // <---- mostra un errore sulle chiamate effettuate
		}

	}

}
