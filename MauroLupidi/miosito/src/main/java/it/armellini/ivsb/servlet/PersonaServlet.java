package it.armellini.ivsb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import it.armellini.ivsb.model.JsonResponse;
import it.armellini.ivsb.model.Persona;
import it.armellini.ivsb.service.FileService;

@WebServlet(urlPatterns = "/persona")
public class PersonaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
    public PersonaServlet() {
        super();
        System.out.println("Wildfly ha costruito PersonaServlet");
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		
		String idClasse = req.getParameter("idClasse");
		
		PrintWriter out = resp.getWriter();
		
		FileService fs = new FileService();
		List<Persona> elencoPersone = fs.persone(idClasse);
		
		Gson gson = new Gson();
		// gson.toJson(elencoPersone);
		out.print(gson.toJson(elencoPersone));
		
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		String strPersona = req.getParameter("datiPersona");
		
		Gson gson = new Gson();
		String strResponse;
		FileService fs = new FileService();
		try {
			Persona personaRequest = gson.fromJson(strPersona, Persona.class);
			fs.salva(personaRequest);
			strResponse = gson.toJson( new JsonResponse("informazione", "Dati aggiornati correttamente", ""));
		} catch(Exception exc) {
			strResponse = gson.toJson(new JsonResponse("errore",
					"Durante l'aggiornamento dei dati si è verificato un errore.", exc.getMessage()));
		}
		
		PrintWriter out = resp.getWriter();
		out.print(strResponse);
		out.flush();
		out.close();
	}

	
}
