import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class MioServer {
	private static int PORT = 9876;

	public static void main(String[] args) {
		boolean endCiclo = false;
		try {
			String messaggio;
			ServerSocket echoSocket = new ServerSocket(PORT);
			do {
				System.out.println("Sono in ascolto sulla porta: " + PORT);
				Socket clientSocket = echoSocket.accept();
				BufferedReader inputBr = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				clientSocket.getInputStream();
				PrintWriter outPw = new PrintWriter(clientSocket.getOutputStream());

				outPw.write("Per favore scrivi qualcosa....\n");
				outPw.flush();

				messaggio = inputBr.readLine();

				while (null != messaggio && !"CIAO".equals(messaggio)) {
					System.out.print("Ho letto " + messaggio);

					outPw.write("===> Arrivederci a presto...\n");
					outPw.flush();
				}
				if ("CIAO".equals(messaggio)) {
					endCiclo = false;
				}
			} while (true);
		} catch (IOException e) {
			System.out.println("Errore durante l'aperturadel socket: ");
		}
	}

}
