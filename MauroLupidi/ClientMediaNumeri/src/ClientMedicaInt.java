import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientMedicaInt {

	static Scanner num1 = new Scanner(System.in);
	static Scanner num2 = new Scanner(System.in);
	static Scanner num3 = new Scanner(System.in);

	public static void main(String[] args) {
		// creare la connessione con il server
		try {
			Socket s = new Socket("localhost", 7890);
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			Writer out = new PrintWriter(s.getOutputStream());
			// s.getInputStream(numScanner); prova a inserire i numeri qui dentro
			
			System.out.println("inserire il primo numero: ");
			int numero1 = num1.nextInt();
			System.out.println("inserire il secondo numero: " );
			int numero2 = num2.nextInt();
			System.out.println("inserire il terzo numero: " );
			int numero3 = num3.nextInt();
	
			
			out.write(numero1);
			out.write(numero2);
			out.write(numero3);
			out.flush();

	

			int risposta = in.read();
			System.out.println("Risposta: " + risposta);

			out.close();
			s.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("ERRORE DURANTE LA CREAZIONE DEL SOCKET: " + e.getMessage());
			System.err.println(e.getMessage());
		}

	}

}
