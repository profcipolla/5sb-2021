create table foto (
id int not null auto_increment primary key,
idAppartamento int not null,
percorsoFile varchar(300) not null
)


create table ruolo (
id int not null auto_increment primary key,
descrizione varchar(100) not null
)
-- lego utente-idRuolo con ruolo.id

alter table utente
add column idRuolo int not null;


alter table utente
add constraint fk_utente_2_ruolo
foreign key (idRuolo)
references ruolo(id);

alter table utente
add column codiceFiscaleProprietario char(16);


alter table utente
add column codiceFiscaleCliente char(16);

alter table 


alter table utente
add constraint fk_utente_2_clienti
foreign key (codiceFiscaleCliente)
references clienti(codiceFiscale);

alter table utente
add constraint fk_utente_2_Proprietari
foreign key (codiceFiscaleProprietario)
references Proprietari(codiceFiscale);

create table Utente (
userName varchar(50) not null,
hashPassord varchar(200) not null,
primary key(username)

)

create table Clienti (
codiceFiscale char(16) not null primary key,
nome varchar(100) not null,
cognome varchar(100) not null,
indirizzo varchar(200) not null,
cellulare varchar(16) not null,
telefono varchar(16),
email varchar(250) not null,
codIstat char(4) not null
)


create table Proprietari(
codiceFiscale char(16) not null primary key,
nome varchar(100) not null,
cognome varchar(100) not null,
indirizzo varchar(200) not null,
cellulare varchar(16) not null,
telefono varchar(16),
email varchar(250) not null,
codIstat char(4) not null,
numeroConto varchar(30)not null


)

create table province(

sigla char(2) not null primary key,
nome varchar(100) not null
)


create table citta (
codIStat char(4) not null primary key,
nome varchar(100) not null,
sigla char(2) not null
)

alter table citta 
add constraint fk_citta_2_province
foreign key (sigla)
references province(sigla)

alter table Proprietari
add constraint fk_Proprietari_2_citta
foreign key (codIstat)
references citta(codIstat)







